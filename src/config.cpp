#include <config.hpp>

Config::Config(std::string filename) : filename(filename) {
    this->load();
};

void Config::load() {
    std::ifstream file(this->filename);

    char key[128], value[128];

    for(std::string line; getline(file, line);){
        sscanf(line.c_str(), "%s = %s", key, value);

        std::cout << "Loaded param: " << key << " = " << value << std::endl;
        this->config.emplace(key, value);
    }
}

void Config::save() {
    std::ofstream file(this->filename);

    char key[128], value[128];

    for (auto it = this->config.begin(); it != this->config.end(); it++){
        std::cout << "Saved param: " << it->first << " = " << it->second << std::endl;
        file << it->first << " = " << it->second << "\n"; 
    }
}

void Config::clean() {
    this->config.clear();
}