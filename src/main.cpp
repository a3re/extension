#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cstdio>

#define MODULE_NAME "library_main"
#include <log.hpp>

#include <extension.hpp>
#include <config.hpp>

#if defined(___WINDOWS___)
    #include <windows.h>
    #define EXPORT __declspec(dllexport)
    #define STDCALL __stdcall
#elif defined(___UNIX___)
    #include <dlfcn.h>
    #define EXPORT
    #define STDCALL
#else
#error "This platform is not supported"
#endif

Extension* ext;
Config* config;

extern "C" {
    EXPORT void STDCALL RVExtension(char* output, int outputSize, const char* function);
    EXPORT int  STDCALL RVExtensionArgs(char* output, int outputSize, const char* function, const char** argv, int argc);
    EXPORT void STDCALL RVExtensionVersion(char* output, int outputSize);
	EXPORT void STDCALL RVExtensionRegisterCallback(int(*callbackProc)(char const *name, char const *function, char const *data));
	EXPORT void STDCALL RVExtensionContext(const char** argv, int argc);

#if defined(___WINDOWS___)
    BOOL APIENTRY DLLMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved);
#endif
}

void STDCALL RVExtension(char* output, int outputSize, const char* function) {
    ext->processCall(output, outputSize, function, NULL, 0);
}

int STDCALL RVExtensionArgs(char* output, int outputSize, const char* function, const char** argv, int argc) {
    return ext->processCall(output, outputSize, function, argv, argc);
}

void STDCALL RVExtensionVersion(char* output, int outputSize) {
    std::strncpy(output, ext->getVersion().c_str(), outputSize - 1);
}

void STDCALL RVExtensionRegisterCallback(int(*callbackProc)(char const *name, char const *function, char const *data)) {
    ext->registerExtensionCallback(callbackProc);
}

void STDCALL RVExtensionContext(const char** argv, int argc) {
    ext->processContext(argv, argc);
}

void mainInit(std::string fullPath) {
    size_t lastSeparatorPos = fullPath.find_last_of("/\\");
    std::string path = fullPath.substr(0, lastSeparatorPos + 1);

    LOG_INFO("Loaded from: %s", path);

    config = new Config(path + "a3re.cfg");

    std::string local_ip = config->get<std::string>("local_ip", "127.0.0.1", true);
    uint32_t local_port = config->get<uint32_t>("local_port", 8385, true);

    ext = new Extension({ local_ip, local_port });
}

void mainFree() {
    LOG_DEBUG("Freeing up resources");
    delete ext;
    delete config;
    LOG_DEBUG("Unloaded");
}

#if defined(___WINDOWS___)
EXTERN_C IMAGE_DOS_HEADER __ImageBase;

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) {
    switch (ul_reason_for_call) {
        case DLL_PROCESS_ATTACH:
            char path[MAX_PATH + 1];
            GetModuleFileNameA((HINSTANCE)&__ImageBase, path, (MAX_PATH + 1));

            mainInit(std::string(path));
            break;

        case DLL_PROCESS_DETACH:
            mainFree();
            break;

        case DLL_THREAD_ATTACH:
        case DLL_THREAD_DETACH:
            break;
    };

    return true;
}
#elif defined(___UNIX___)
__attribute__((constructor)) void load() {
    Dl_info dl_info;
	dladdr((void*)load, &dl_info);

    mainInit(std::string(dl_info.dli_fname));
}

__attribute__((destructor)) void free() {
    mainFree();
}
#else
#error "This platform is not supported"
#endif