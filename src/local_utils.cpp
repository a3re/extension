#include <local_utils.hpp>

std::string removeQuotes(std::string str) {
    std::string result = str;

    if (!result.empty() && result.front() == '"')
        result.erase(0, 1);

    if (!result.empty() && result.back() == '"')
        result.pop_back();

    return result;
}