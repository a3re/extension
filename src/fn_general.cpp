#include <fn_general.hpp>
#include <extension.hpp>
#include <config.hpp>

extern Extension* ext;
extern Config* config;

Caller::Return_t fn_getVersion(std::vector<std::string> args, void* ptr) {
    return {
        .out = ext->getVersion(),
        .ret = Caller::Status_t::OK,
    };
}

Caller::Return_t fn_isConnected(std::vector<std::string> args, void* ptr) {
    return {
        .out = ext->isConnected() ? "true" : "false",
        .ret = Caller::Status_t::OK,
    };
}

Caller::Return_t fn_connect(std::vector<std::string> args, void* ptr) {
    std::string remote_ip = config->get<std::string>("remote_ip", "127.0.0.1", true);
    uint32_t remote_port = config->get<uint32_t>("remote_port", 8386, true);

    if (ext->connect({ remote_ip, remote_port }))
        return {
            .out = "Connected",
            .ret = Caller::Status_t::OK,
        };

    return {
        .out = "Failed to connect",
        .ret = Caller::Status_t::INTERNAL_ERROR,
    };
}

Caller::Return_t fn_disconnect(std::vector<std::string> args, void* ptr) {
    ext->disconnect();

    return {
        .out = "Disconnected",
        .ret = Caller::Status_t::OK,
    };
}

Caller::Return_t fn_bitwise(std::vector<std::string> args, void* ptr) {
    if (args.size() < 3)
        return {
            .out = "Not enough arguments",
            .ret = Caller::Status_t::NOT_ENOUGH_ARGUMENTS,
        };

    uint32_t op = std::stoi(args[0]);
    uint32_t a = std::stoi(args[1]);
    uint32_t b = std::stoi(args[2]);
    uint32_t c = 0;

    switch (op) {
        case 0: c = a & b; break;
        case 1: c = a | b; break;
        case 2: c = a ^ b; break;

        default:
            return {
                .out = "Invalid operation",
                .ret = Caller::Status_t::INTERNAL_ERROR,
            };
    }

    return {
        .out = std::to_string(c),
        .ret = Caller::Status_t::OK,
    };
}

void fn_general_init(Caller* caller) {
    caller->addFunction("VERSION", fn_getVersion);
    caller->addFunction("isConnected", fn_isConnected);

    caller->addFunction("connect", fn_connect);
    caller->addFunction("disconnect", fn_disconnect);

    caller->addFunction("bitwise", fn_bitwise);
}