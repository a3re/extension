#include <extension.hpp>

#define MODULE_NAME "Extension"
#include <log.hpp>

#if defined(_X64)
    #define __ARCH__ "x64"
#elif defined(_X86)
    #define __ARCH__ "x86"
#else
    #define __ARCH__ "UNKNOWN"
#endif

Extension::Extension(UDPNode::node_t node) : remote_uid(0) {
    LOG_INFO("Architecture: " __ARCH__);

    caller = new Caller();
    remoteCaller = new Caller();

    rc = new RemoteCall(remoteCaller, Node::type_t::NODE_TYPE_EXTENSION, node);
    LOG_INFO("Remote call subsystem version: %s", rc->getVersion());

    fn_general_init(caller);
}

Extension::~Extension() {
    delete rc;

    delete remoteCaller;
    delete caller;
}

bool Extension::connect(UDPNode::node_t node) {
    uint32_t uid = rc->connect(node);

    uint8_t attempts = 0;

    while (!rc->isConnected(uid)) {
        if (rc->isRejected(uid) || attempts >= 30)
            return false;

        std::this_thread::yield();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        attempts++;
    }

    this->remote_uid = uid;
    return true;
}

void Extension::disconnect() {
    rc->disconnect(this->remote_uid);
    this->remote_uid = 0;
}

bool Extension::isConnected() {
    return rc->isConnected(this->remote_uid);
}

void Extension::registerExtensionCallback(arma3_ext_callback_t cb) {
    LOG_INFO("Extension callback registred!");
    this->extCallback = cb;
}

int Extension::processCall(char* output, int outputSize, const char* name, const char** argv, int argc) {
    std::vector<std::string> args(argv, argv + argc);

    for (auto it = args.begin(); it != args.end(); it++) {
        *it = removeQuotes(*it);
    }

    Caller::Return_t ret = this->_processCall(std::string(name), args);

    std::strncpy(output, ret.out.c_str(), outputSize - 1);

    return ret.ret;
}

Caller::Return_t Extension::_processCall(std::string name, std::vector<std::string> args) {
    LOG_DEBUG("Called: %s", name);

    std::regex pattern("remote\\(([^)]+)\\)");
    std::smatch matches;

    if (std::regex_search(name, matches, pattern) && matches.size() == 2) {
        uint32_t calluid = this->rc->call(this->remote_uid, matches[1], args);

        if (!calluid)
            return { 
                .out = "Failed to initiate call",
                .ret = Caller::Status_t::INTERNAL_ERROR,
            };
        

        return this->rc->getCallRet(this->remote_uid, calluid, 2000);
    }
    
    return this->caller->process(name, args, this->rc);
}

void Extension::processContext(const char** argv, int argc) {
    // NOT IMPLEMENTED YET
    
    // log << Log::Level::INFO << "processContext" << std::endl;

    // for (int i = 0; i < argc; ++i) {
    //     log << argv[i] << std::endl;
    // }
}