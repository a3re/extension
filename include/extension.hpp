#pragma once

#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <iostream>
#include <cstdio>
#include <regex>

#include <local_utils.hpp>

#include <caller.hpp>

#include <fn_general.hpp>
#include <remoteCall.hpp>

typedef std::function<int(char const *name, char const *function, char const *data)> arma3_ext_callback_t;

class Extension {
    public:
        Extension(UDPNode::node_t node);
        ~Extension();

        static std::string getVersion() {
            return tfm::format("%s.%s", EXTENSION_VERSION_MAJOR, EXTENSION_VERSION_MINOR);
        }

        bool connect(UDPNode::node_t node);
        void disconnect();
        bool isConnected();

        void registerExtensionCallback(arma3_ext_callback_t cb);
        int processCall(char* output, int outputSize, const char* __name, const char** argv, int argc);
        void processContext(const char** argv, int argc);

    private:
        Caller* caller;
        Caller* remoteCaller;

        RemoteCall* rc;

        uint32_t remote_uid;

        arma3_ext_callback_t extCallback = nullptr;

        Caller::Return_t _processCall(std::string name, std::vector<std::string> args);
};