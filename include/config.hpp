#pragma once

#include <cstdint>
#include <iostream>
#include <string>
#include <fstream>
#include <string>
#include <map>
#include <stdexcept>

#include <tinyformat.h>

class Config {
    public:
        Config(std::string filename);

        void load();
        void save();
        void clean();

        template<typename U>
        U get(std::string key, U def, bool set) {
            auto it = this->config.find(key);
            if (it != this->config.end()) {
                try {
                    return convert<U>(it->second);
                } catch (std::exception& e) {
                    std::cerr << "Error converting value for key '" << key << "': " << e.what() << std::endl;
                    throw;
                }
            } 
            
            if (set)
                this->set(key, def);

            return def;
        }

        template<typename U>
        void set(std::string key, U value) {
            this->config[key] = convertToString(value);
        }

    private:
        template<typename U>
        U convert(std::string& str) {
            U result;
            std::istringstream iss(str);
            iss >> result;
            if (iss.fail() || !iss.eof()) {
                throw std::invalid_argument("Failed to convert string to specified type");
            }
            return result;
        }

        template<typename U>
        std::string convertToString(U& value) {
            std::ostringstream oss;
            oss << value;
            return oss.str();
        }

        std::string filename;
        std::map<std::string, std::string> config;
};