#pragma once

#include <vector>
#include <string>
#include <cstring>

#include <caller.hpp>
#include <remoteCall.hpp>

void fn_general_init(Caller* caller);